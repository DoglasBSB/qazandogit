package StepDefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pagefactory.LoginPages_PF;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class LoginSteps_PF {

    WebDriver driver = null;
    LoginPages_PF login, link;
    //LoginPages_PF link;

    @Deprecated
    @Before
    public void browserSetup (){

            System.out.println("Iniciando Browser");
            System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
            driver = new ChromeDriver();

            driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }

   @After
    public void teardown () {
        System.out.println("Fechando Browser");
        driver.close();
        driver.quit();
    }

    // LOGIN
    @Dado("que estou na página de login")
    public void queEstouNaPáginaDeLogin() {
        System.out.println("Página de Login");
        driver.navigate().to("https://opensource-demo.orangehrmlive.com/");
    }

    @Quando("^me autentico com um (.*) e (.*) válidos$")
    public void meAutenticoComUmNomeESenhaVálidos(String username, String password) throws InterruptedException {

        login = new LoginPages_PF(driver);
        login.enterUsername(username);
        login.enterPassword(password);

        Thread.sleep(2000);
    }

    @E("clico em login")
    public void clicoEmLogin() {
        login.clickOnLogin();
    }

    @Então("sou direcionado para página principal")
    public void souDirecionadoParaPáginaPrincipal() throws InterruptedException {
        System.out.println("Bem vindo a Home Page");

        //Válida a mensagem de sucesso na página principal
        assertThat(driver.findElement(By.cssSelector("#content > div > div.head > h1")).getText(), is("Dashboard"));

        Thread.sleep(2000);
    }

    //SENHA
    @Quando("clico em Esqueceu sua senha")
    public void clicoEmEsqueceuSuaSenha() {
        //LoginPages_PF link = new LoginPages_PF(driver);
        //link.clickLinkSenha();
         //driver.findElement(By.id("forgotPasswordLink")).click();
       // driver.findElement(By.linkText("index.php/auth/requestPasswordResetCode")).click();
       driver.findElement(By.cssSelector("#forgotPasswordLink > a")).click();

    }


    @Então("sou direcionado para pagina de redefinir senha")
    public void souDirecionadoParaPaginaDeRedefinirSenha() throws InterruptedException{
        System.out.println("Bem vindo a Home Page");

        //Válida a mensagem de sucesso na página de redefinição de senha
        //assertThat(driver.findElement(By.className("head")).getText(), is("Forgot Your Password?"));
        //assertThat(driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/h1")).getText(), is("Forgot Your Password?"));
        //assertThat(driver.findElement(By.linkText("Forgot your password?")), is("Forgot Your Password?"));


        //Válida a mensagem na página de Reset
        assertThat(driver.findElement(By.cssSelector("#content > div > div.head > h1")).getText(), is("Forgot Your Password?"));


        //xpath=//div[@id='content']/div/div[2]/h1
        Thread.sleep(2000);
    }

}
