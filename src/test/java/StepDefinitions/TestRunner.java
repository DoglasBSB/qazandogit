package StepDefinitions;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features= "src/test/resources/Features/login.feature",
        glue={"StepDefinitions"},
        plugin = { "pretty","json:target/cucumber.json ",
                  "html:target/HtmlReports/Reports.html"},
        monochrome = true,
        tags="@login or @esqueci"
        //tags="@esqueci and @login"
        //tags="@login and not @esqueci"
        //tags = "(@esqueci or @email) and not @login"

        //Single tag -> //tags="@login
        //tags = "@login or @email"
        //Multiple tags -> = "@smoke or @regression"
        //AND OR tags = "@smoke and @regression"
        //Ignore tags -> "@smoke and not @regression"
        // tags = "(@smoke or @regression) and not @important"
        // tags = "(@login or @esqueci) and @senha"

)
public class TestRunner {

}
