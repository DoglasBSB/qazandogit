package pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocator;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import java.util.List;

public class LoginPages_PF {

    //Locators
    @FindBy(id = "txtUsername")
    @CacheLookup
    WebElement txt_username;

    @FindBy(id = "txtPassword")
    WebElement txt_password;

    @FindBy(id = "btnLogin")
    WebElement btn_login;

    private By link_senha = By.id("forgotPasswordLink");


    @FindBy(partialLinkText = "Admin")
     List <WebElement> myLinks;

    @FindBy(how = How.ID, using = "name")
    WebElement abc;

    //Constructor
    WebDriver driver;

    public LoginPages_PF(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 30), this);
        //PageFactory.initElements(driver, LoginPages_PF.class);

    }

    //Methods
    public void enterUsername(String username) {
        txt_username.sendKeys(username);
    }

    public void enterPassword(String password) {
        txt_password.sendKeys(password);
    }

    public void clickOnLogin() {
        btn_login.click();
    }

    public void clickLinkSenha() {
       //link_senha.click();
       driver.findElement(link_senha);
    }

}
