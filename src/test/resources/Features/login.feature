#Autor: Francisco Dôglas
#Data: 02/06/2022
#Descrição:  Verificar a funcionalidade da página de login
#language: pt

@smokeFeatute
Funcionalidade:Login Realizar login no sistema de gestão
  Eu como usuário
  Quero realizar login no Sistema da OrangeHRM
  Para ter acesso ao gerenciamento de recursos humanos

  Contexto: possibilidade de acessar o sistema
    Dado que estou na página de login

# Fluxo Principal

  @login
  Esquema do Cenário: Verificar se o login foi bem-sucedido com credenciais válidas
    Quando me autentico com um <nome> e <senha> válidos
    E clico em login
    Então sou direcionado para página principal

    Exemplos:
      | nome | senha |
      | Admin | admin123 |

    # Fluxo alternativo

  @esqueci
  Cenário: Esqueci minha senha
    Quando clico em Esqueceu sua senha
    Então sou direcionado para pagina de redefinir senha