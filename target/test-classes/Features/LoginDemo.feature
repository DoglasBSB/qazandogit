#Autor: Francisco Dôglas
#Data: 02/06/2022
#Descrição:  Verificar a funcionalidade da página de login
#language: pt

#@smokefeature
Funcionalidade:Testar a funcionalidade de login
  Eu como usuário
  Quero realizar login na minha conta da OrangeHRM
  Para ter acesso ao gerenciamento de recursos humanos

  Contexto:
    Dado que estou na página de login

# Fluxo Principal

  @login
  Esquema do Cenário: Verificar se o login foi bem-sucedido com credenciais válidas
    Quando me autentico com um <username> e <password> válidos
    Então sou direcionado para página principal
    Exemplos:
      | username | password |
      | @Admin| admin123 |

# Fluxo alternativo

  @esqueci
  Cenário: Esqueci minha senha
    Quando clico em Esqueceu sua senha?
    Então sou direcionado para pagian de redefinir senha


# Fluxo de exceção

  @usuario
  Cenário:Usuário não informado
    Quando dou continuidade sem o preenchimento do usuario
    Então devo ver uma mensagem Username cannot be empty

  @senha
  Cenário: Senha não informada
    Quando dou continuidade sem o preenchimento da senha
    Então devo ver uma mensagem Password cannot be empty

  @deslogar
  Cenário: Deslogar do sistema
    Quando clico em Logout
    Então devo ser deslogado do sistema

